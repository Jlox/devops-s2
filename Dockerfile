FROM node:12
# Create app directory
WORKDIR ./

# Copy package
COPY . .

# Port to expose documentation
EXPOSE 3000/tcp

# Prepare environment
RUN npm install
RUN npm run seed
CMD npm run start


